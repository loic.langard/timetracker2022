﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeTracker.Apps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Apps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddTimerPage : ContentPage
    {
        AddTimerViewModel vm;
        public AddTimerPage()
        {
            InitializeComponent();
            vm = new AddTimerViewModel();
            NavigationPage.SetHasBackButton(this, false);
            BindingContext = vm;
        }
    }
}