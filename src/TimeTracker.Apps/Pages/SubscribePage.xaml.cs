﻿using TimeTracker.Apps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Apps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SubscribePage : ContentPage
    {
        private SubscribePageViewModel subVM;

        public SubscribePage()
        {
            InitializeComponent();
            subVM = new SubscribePageViewModel();
            NavigationPage.SetHasBackButton(this, false);
            BindingContext = subVM;
            
        }
    }
}