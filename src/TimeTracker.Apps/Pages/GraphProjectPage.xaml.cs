﻿using Microcharts;
using MonkeyCache.FileStore;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeTracker.Apps.ViewModels;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.ChartEntry;

namespace TimeTracker.Apps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GraphProjectPage : ContentPage
    {
        GraphProjectViewModel vm;
        public GraphProjectPage()
        {
            InitializeComponent();
            vm = new GraphProjectViewModel();
            NavigationPage.SetHasBackButton(this, false);
            BindingContext = vm;

        }
    }
}