﻿using TimeTracker.Apps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeTracker.Apps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        LoginPageViewModel logVM;
        public LoginPage()
        {
            InitializeComponent();
            logVM = new LoginPageViewModel();
            NavigationPage.SetHasBackButton(this, false);
            BindingContext = logVM;
        }
    }
}