﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class ModifyTimeViewModel : ViewModelBase
    {
        public ICommand ModifyStartCommand { get; }
        public ICommand ModifyStopCommand { get;}

        private DateTime debut;
        private DateTime fin;

        public ModifyTimeViewModel()
        {
            ModifyStopCommand = new Command(StopTimer);
            ModifyStartCommand = new Command(StartTimer);
        }

        private void StartTimer(object obj)
        {
            debut = DateTime.Now;
        }

        private async void StopTimer(object obj)
        {
            fin = DateTime.Now;

            DateTime start=Barrel.Current.Get<DateTime>(key: "Start_time");
            DateTime stop = Barrel.Current.Get<DateTime>(key: "End_time");
            long timeId = Barrel.Current.Get<long>(key: "TimeID");
            
            


            AddTimeRequest addTimeRequest = NewAddTimeRequest(start,stop);


            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            long pid = Barrel.Current.Get<long>(key: "ProjectID");
            long tid = Barrel.Current.Get<long>(key: "TaskID");

            
            var method = new HttpMethod("PUT");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.UPDATE_TIME.Replace("{projectId}", pid.ToString()).Replace("{taskId}", tid.ToString()).Replace("{timeId}", timeId.ToString()))
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(addTimeRequest),
                    Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<TaskPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Suppression temps echouée (" + response.StatusCode + ")", "OK", "cancel");

                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<TaskPage>();
            }


        }

        private AddTimeRequest NewAddTimeRequest(DateTime start, DateTime stop)
        {
            AddTimeRequest addTimeRequest = new AddTimeRequest();
            addTimeRequest.StartTime = start;

            
            TimeSpan diff2 = DateTime.Now.Subtract(debut);

            addTimeRequest.EndTime = stop.Add(diff2);

            

            
            
            return addTimeRequest;
        }


    }
}
