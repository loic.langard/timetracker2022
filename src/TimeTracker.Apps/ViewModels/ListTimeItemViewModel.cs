﻿using MonkeyCache.FileStore;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class ListTimeItemViewModel : ViewModelBase
    {
        public ObservableCollection<Time> TimeItems { get; set; }
        public ICommand AddTimeItemCommand { get; }
        public ListTimeItemViewModel()
        {
            AddTimeItemCommand = new Command(AddTimeCommand);
            TimeItems = new ObservableCollection<Time>();
            BackToTaskCommand = new Command(ListTaskCommand);
            LoadItems();
        }

        private void ListTaskCommand(object obj)
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<TaskPage>();
        }

        private void LoadItems()
        {
            List<Time> times = Barrel.Current.Get<List<Time>>(key: "TimeCollec");
            foreach (Time time in times)
            {
                time.SetCommand(new Command<Time>(DeleteTimeCommand), new Command<Time>(ModifyTimeCommand));
                TimeItems.Add(time);
            }
        }

        private async void ModifyTimeCommand(Time obj)
        {
            DateTime st = obj.Start_time;
            DateTime stp = obj.End_time;
            long id = obj.Id;
            Barrel.Current.Add(key: "Start_time", data: st, expireIn: TimeSpan.FromHours(2));
            Barrel.Current.Add(key: "End_time", data: stp, expireIn: TimeSpan.FromHours(2));
            Barrel.Current.Add(key: "TimeID", data: id, expireIn: TimeSpan.FromHours(2));
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<ModifyTimePage>();
        }
        private async void DeleteTimeCommand(Time obj)
        {
            long pid = Barrel.Current.Get<long>(key: "ProjectID");
            long tid = Barrel.Current.Get<long>(key: "TaskID");
            long tmid = obj.Id;
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            var method = new HttpMethod("DELETE");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.DELETE_TIME.Replace("{projectId}", pid.ToString()).Replace("{taskId}", tid.ToString()).Replace("{timeId}",tmid.ToString()))
            {
                Content = null
            };
            var response = await client.SendAsync(request);
            string res = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<TaskPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Error" + response.StatusCode + ")", "OK", "cancel");
            }
        }

        private async void AddTimeCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<TimePage>();
        }

        

        

        public ICommand BackToTaskCommand
        {
            get;
        }

        
    }
}
