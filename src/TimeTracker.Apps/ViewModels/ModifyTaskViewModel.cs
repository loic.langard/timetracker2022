﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class ModifyTaskViewModel : ViewModelBase
    {

        private string name;

        public string Name { get => name; set => SetProperty(ref name, value); }

        private long pid;
        private long tid;

        public ICommand SaveCommand { get; }

        public ModifyTaskViewModel()
        {
            Name = Barrel.Current.Get<string>(key: "TaskName");
            pid = Barrel.Current.Get<long>(key: "ProjectID");
            tid = Barrel.Current.Get<long>(key: "TaskID");

            SaveCommand = new Command(saveModifiation);

        }

        private void saveModifiation(object obj)
        {
            if (string.IsNullOrEmpty(Name))
                App.Current.MainPage.DisplayAlert("Champs non renseignés", "Erreur", "ok");
            else
            {
                ModifierAsync();
            }
        }

        private async void ModifierAsync()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
           
            AddTaskRequest addTaskRequest = AddTaskRequest();

            var method = new HttpMethod("PUT");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.UPDATE_TASK.Replace("{projectId}",pid.ToString()).Replace("{taskId}",tid.ToString()))
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(addTaskRequest),
                    Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);

           

            
            if (response.IsSuccessStatusCode)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<TaskPage>();
            }
            else
            {

                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Modification du compte echouée (" + response.StatusCode + ")", "OK", "cancel");
            }


        }

        private AddTaskRequest AddTaskRequest()
        {
            AddTaskRequest addTaskRequest = new AddTaskRequest();
            addTaskRequest.Name = name;
            return addTaskRequest;
        }
    }
}
