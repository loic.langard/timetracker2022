using Storm.Mvvm;
using System.Windows.Input;
using Storm.Mvvm.Services;
using Xamarin.Forms;
using TimeTracker.Apps.Pages;

namespace TimeTracker.Apps.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public ICommand PageSuivante { get; }
        public ICommand Inscrire { get; }


        public MainViewModel()
        {
            PageSuivante = new Command(Connexion);
            Inscrire = new Command(Inscription);

        }

        public void Connexion()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<LoginPage>();
        }

        public void Inscription()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<SubscribePage>();
        }

    }

}
