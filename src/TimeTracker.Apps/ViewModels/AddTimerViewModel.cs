﻿using MonkeyCache.FileStore;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class AddTimerViewModel : ViewModelBase
    {
        public ICommand StopCommand { get; }

        private DateTime debut;
        private DateTime fin;

        public AddTimerViewModel()
        {
            StopCommand = new Command(StopTimer);
            debut = DateTime.Now;
        }

        private void StopTimer(object obj)
        {
            fin = DateTime.Now;

            Barrel.Current.Add(key: "timeDebut", data: debut, expireIn: TimeSpan.FromHours(2));
            Barrel.Current.Add(key: "timeFin", data: fin, expireIn: TimeSpan.FromHours(2));

            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<AddTimerProjectListPage>();
        }
    }
}
