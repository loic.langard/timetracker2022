﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class ModifyProjectViewModel : ViewModelBase
    {
        private string description;
        private string name;
        private long id;

        public ICommand SaveCommand { get; }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public ModifyProjectViewModel()
        {
            description = Barrel.Current.Get<string>(key:"Description");
            name = Barrel.Current.Get<string>(key: "Name");
            id = Barrel.Current.Get<long>(key: "ProjectID");
            SaveCommand = new Command(saveModification);

        }

        private void saveModification(object obj)
        {
            if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Description))
                App.Current.MainPage.DisplayAlert("Champs non renseignés", "Erreur", "ok");
            else
            {
                ModifierAsync();
            }
        }

        private async void ModifierAsync()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            AddProjectRequest addProjectRequest=AddProjectRequest();
            
            var method = new HttpMethod("PUT");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.UPDATE_PROJECT.Replace("{projectId}", id.ToString()))
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(addProjectRequest),
                    Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);

            
            if (response.IsSuccessStatusCode)
            {
                
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MenuProjectPage>();
            }
            else
            {

                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Modification du compte echouée (" + response.StatusCode+")", "OK", "cancel");
            }


        }

        private AddProjectRequest AddProjectRequest()
        {
            AddProjectRequest addProjecRequest = new AddProjectRequest();
            addProjecRequest.Name = name;
            addProjecRequest.Description = description;
            return addProjecRequest;
        }

        private Command goBackCommand;

        public ICommand GoBackCommand
        {
            get
            {
                if (goBackCommand == null)
                {
                    goBackCommand = new Command(GoBack);
                }

                return goBackCommand;
            }
        }

        private async void GoBack()
        {
            INavigationService nav = DependencyService.Get<INavigationService>();
            await nav.PushAsync<MenuProjectPage>();
        }
    }
}
