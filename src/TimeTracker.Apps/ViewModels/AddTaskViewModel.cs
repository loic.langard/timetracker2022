﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class AddTaskViewModel : ViewModelBase
    {
        private string name;
        public ICommand SaveCommand { get; }
        public ICommand GoBackCommand { get; }
        public string Name { get => name; set => SetProperty(ref name, value); }
        public AddTaskViewModel()
        {
            SaveCommand = new Command(SaveProjectCommand);
            GoBackCommand = new Command(GoToMenuTaskCommand);
        }
        private void SaveProjectCommand()
        {
            if (string.IsNullOrEmpty(Name))
                App.Current.MainPage.DisplayAlert("Erreur", " Champ(s) non renseigné(s).", "ok");
            else
            {
                SaveTask();
            }
        }
        private async void SaveTask()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);

            AddTaskRequest addTaskRequest=NewAddTaskRequest();
            var json = JsonConvert.SerializeObject(addTaskRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            long id = Barrel.Current.Get<long>(key: "ProjectID");
            var responseMessage = await client.PostAsync(Urls.HOST + "/" + Urls.CREATE_TASK.Replace("{projectId}",id.ToString()), data);
            
            if (responseMessage.IsSuccessStatusCode)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<TaskPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Echec","OK", "Cancel");
            }

            

        }
        private AddTaskRequest NewAddTaskRequest()
        {
            AddTaskRequest addTaskRequest = new AddTaskRequest();
            addTaskRequest.Name = name;
            return addTaskRequest;
        }
        private void GoToMenuTaskCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<TaskPage>();
        }
    }
}
