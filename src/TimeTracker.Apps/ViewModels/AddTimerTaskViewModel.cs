﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class AddTimerTaskViewModel: ViewModelBase
    {

        private long projectId;
        public ObservableCollection<Task> Tasks { get; set; }
        public ICommand AddCommand { get; }
        public ICommand BackToProjectCommand { get; }

        public long ProjectId
        {
            get => projectId;
            set => SetProperty(ref projectId, value);
        }
        public AddTimerTaskViewModel()
        {
            AddCommand = new Command(AddTaskCommand);
            BackToProjectCommand = new Command(GoBackCommand);
            Tasks = new ObservableCollection<Task>();
            LoadTasks();
        }

        private void GoBackCommand(object obj)
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<AddTimerProjectListPage>();
        }

        private void AddTaskCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<AddTimerAddTaskPage>();
        }
        
        private async void LoadTasks()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            projectId = Barrel.Current.Get<long>(key: "ProjectID");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            var responseMessage = await client.GetAsync(Urls.HOST + "/" + Urls.LIST_TASKS.Replace("{projectId}", projectId.ToString()));
            string res = await responseMessage.Content.ReadAsStringAsync();
            Response<List<TaskItem>> response = JsonConvert.DeserializeObject<Response<List<TaskItem>>>(res);
            List<TaskItem> taskItems = response.Data;
            Barrel.Current.Add(key: "ListTaskItem", data: taskItems, expireIn: TimeSpan.FromHours(2));
            if (responseMessage.IsSuccessStatusCode)
            {
                foreach (TaskItem task in taskItems)
                {
                    Task ts = new Task(new Command<Task>(SelectCommand), new Command<Task>(DeleteTaskCommand), new Command<Task>(ModifyTaskCommand));
                    ts.Id = task.Id;
                    ts.Name = task.Name;
                    foreach (TimeItem time in task.Times)
                    {
                        Time t = new Time(time.Id, time.StartTime, time.EndTime);
                        ts.Times.Add(t);
                        ts.Calcultime();
                    }

                    Tasks.Add(ts);
                }
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Echec", "OK", "cancel");

            }
        }

        private void ModifyTaskCommand(Task obj)
        {
            
        }

        private async void SelectCommand(Task obj)
        {
            AddTimeRequest addTimeRequest = NewAddTimeRequest();


            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            long pid = Barrel.Current.Get<long>(key: "ProjectID");
            long tid = obj.Id;

            var json = JsonConvert.SerializeObject(addTimeRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var responseMessage = await client.PostAsync(Urls.HOST + "/" + Urls.ADD_TIME.Replace("{projectId}", pid.ToString()).Replace("{taskId}", tid.ToString()), data);
            if (responseMessage.IsSuccessStatusCode)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MenuMePage>();
            }
            else
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("ajout echouée (" + responseMessage.StatusCode + ")", "OK", "cancel");
                await nav.PushAsync<MenuMePage>();
            }

        }

        private AddTimeRequest NewAddTimeRequest()
        {
            AddTimeRequest addTimeRequest = new AddTimeRequest();
            addTimeRequest.EndTime = Barrel.Current.Get<DateTime>(key: "timeFin");
            addTimeRequest.StartTime = Barrel.Current.Get<DateTime>(key: "timeDebut");
            return addTimeRequest;
        }

        private async void DeleteTaskCommand(Task obj)
        {
            long id = Barrel.Current.Get<long>(key: "ProjectID");
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            var method = new HttpMethod("DELETE");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.DELETE_TASK.Replace("{projectId}", id.ToString()).Replace("{taskId}", obj.Id.ToString()))
            {
                Content = null
            };
            var response = await client.SendAsync(request);
            string res = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<AddTimerTaskPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Suppression de la tâche echouée (" + response.StatusCode + ")", "OK", "cancel");
            }
        }
        
    }
}
