﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace TimeTracker.Apps.ViewModels
{
    class Task : NotifierBase
    {
        private long id;
        private string name;
        private string totalTime;
        private List<Time> times;
        public ICommand TimeCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ModifyCommand { get; set; }
        public Task(ICommand time, ICommand deleteCommand, ICommand modifyCommand)
        {
            TimeCommand = time;
            DeleteCommand = deleteCommand;
            ModifyCommand = modifyCommand;
            times = new List<Time>();
        }

        public string Calcultime()
        {
            TimeSpan d;
            TimeSpan curr = new TimeSpan(0, 0, 0, 0);
            foreach (Time t in times)
            {
                d = d + t.Total_sec;
            }
            totalTime = d.ToString();
            return d.ToString();
        }

        public long Id
        {
            get => id;
            set => SetProperty(ref id, value);
        }
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
        public List<Time> Times
        {
            get => times;
            set => SetProperty(ref times, value);
        }

        

        public string TotalTime { get => totalTime; set => SetProperty(ref totalTime, value); }
    }
}
