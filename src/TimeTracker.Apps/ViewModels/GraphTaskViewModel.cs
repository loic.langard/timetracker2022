﻿using Microcharts;
using MonkeyCache.FileStore;
using SkiaSharp;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;
using Entry = Microcharts.ChartEntry;

namespace TimeTracker.Apps.ViewModels
{
    class GraphTaskViewModel : ViewModelBase
    {
        public ICommand GoBackCommand { get; }
        public GraphTaskViewModel()
        {

            List<TaskItem> taskItems = Barrel.Current.Get<List<TaskItem>>(key: "ListTaskItem");
            int i = taskItems.Count;
            var random = new Random();
            var entries = new Entry[i];
            int j = -1;
            foreach (TaskItem projet in taskItems)
            {
                j++;
                var color = String.Format("#{0:X6}", random.Next(0x1000000));
                var val = projet.Times;
                double value=0;
                foreach (TimeItem ti in val)
                {
                    DateTime start=ti.StartTime;
                    DateTime stop = ti.EndTime;
                    TimeSpan timeSpan = stop.Subtract(start);
                    value += timeSpan.TotalSeconds;
                }
                entries[j] = new Entry(unchecked((int)value))
                {
                    Label = projet.Name,
                    ValueLabel = value.ToString(),
                    Color = SKColor.Parse(color)
                };
            }
            chartTask = new DonutChart { Entries = entries };
            GoBackCommand = new Command(GoToMenuTaskCommand);
        }
        private async void GoToMenuTaskCommand()
        {
            INavigationService nav = DependencyService.Get<INavigationService>();
            await nav.PushAsync<TaskPage>();
        }
        private Chart chartTask;
        public Chart ChartTask { get => chartTask; set => SetProperty(ref chartTask, value);}
    }
}
