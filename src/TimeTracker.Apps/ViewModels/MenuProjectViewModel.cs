﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using SkiaSharp;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;
using Microcharts;
using Entry = Microcharts.ChartEntry;

namespace TimeTracker.Apps.ViewModels
{
    class MenuProjectViewModel : ViewModelBase
    {
        public ICommand AddCommand { get; }
        public ICommand GraphProjectCommand { get; }

        public ICommand BackToMeCommand { get; }
        public ObservableCollection<Projet> Projects { get; set; }
        public MenuProjectViewModel()
        {
            AddCommand = new Command(AddProjectCommand);
            GraphProjectCommand = new Command(GraphCommand);
            BackToMeCommand = new Command(GoToMeCommand);
            Projects = new ObservableCollection<Projet>();
            LoadProjects();
        }

        private async void GoToMeCommand(object obj)
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<MenuMePage>();
        }

        private async void GraphCommand()
        {
            List<ProjectItem> projectItems=Barrel.Current.Get<List<ProjectItem>>(key: "ListProjectItem");
           

            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<GraphProjectPage>();

        }

        private void AddProjectCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<AddProjectPage>();
        }

        private async void LoadProjects()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            var responseMessage = await client.GetAsync(Urls.HOST + "/" + Urls.LIST_PROJECTS);
            string res = await responseMessage.Content.ReadAsStringAsync();
            Response<List<ProjectItem>> response = JsonConvert.DeserializeObject<Response<List<ProjectItem>>>(res);
            List<ProjectItem> projectItems= response.Data;
            Barrel.Current.Add(key: "ListProjectItem", data: projectItems, expireIn: TimeSpan.FromHours(2));
            foreach (ProjectItem projet in projectItems)
            {
                Projet pr = new Projet(new Command<Projet>(ListTaskCommand),new Command<Projet>(DeleteProjectCommand),new Command<Projet>(ModifyProjectCommand));
                pr.Id = projet.Id;
                pr.Name = projet.Name;
                pr.Description = projet.Description;
                pr.TotalSeconds = projet.TotalSeconds;
                Projects.Add(pr);
            }
        }
        private async void ListTaskCommand(Projet obj)
        {
            Barrel.Current.Add(key: "ProjectID", data: obj.Id, expireIn: TimeSpan.FromHours(2));

            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<TaskPage>();
        }
        private async void ModifyProjectCommand(Projet obj)
        {
            Barrel.Current.Add(key: "Description", data: obj.Description, expireIn: TimeSpan.FromHours(2));
            Barrel.Current.Add(key: "Name", data: obj.Name, expireIn: TimeSpan.FromHours(2));
            Barrel.Current.Add(key: "ProjectID", data: obj.Id, expireIn: TimeSpan.FromHours(2));


            INavigationService navigationService = DependencyService.Get<INavigationService>();
            await navigationService.PushAsync<ModifyProjectPage>();




        }
        private async void DeleteProjectCommand(Projet obj)
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            

            var method = new HttpMethod("DELETE");
            var request = new HttpRequestMessage(method, Urls.HOST + "/" + Urls.DELETE_PROJECT.Replace("{projectId}", obj.Id.ToString()))
            {
                Content = null
            };
            var response = await client.SendAsync(request);


            
            if (response.IsSuccessStatusCode)
            {

                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MenuProjectPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Suppression du projet echouée (" + response.StatusCode + ")", "OK", "cancel");
            }
        }

        
    }
}
