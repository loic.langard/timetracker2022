﻿using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Authentications.Credentials;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private string userName;
        public string UserName
        {
            get =>userName;
            set =>SetProperty(ref userName,value);
        }
        private string password;
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
            

        public LoginPageViewModel()
        {
            LoginCommand = new Command(CheckField);
            SubscribeCommand = new Command(GoToSubscribeCommand);
        }

        public ICommand LoginCommand { get; }

        public ICommand SubscribeCommand { get; }
        

        public void GoToSubscribeCommand()
        {
            INavigationService nav = DependencyService.Get<INavigationService>();
            nav.PushAsync<SubscribePage>();
        }

        private void CheckField()
        {
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
                App.Current.MainPage.DisplayAlert("Erreur", " Champ(s) non renseigné(s).", "ok");
            else
            {
                Login();
            }

        }

        public async void Login()
        {
            LoginWithCredentialsRequest createUserRequest = NewLoginWithCredentialsRequest();
            var json = JsonConvert.SerializeObject(createUserRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            var responseMessage = await client.PostAsync(Urls.HOST + "/" + Urls.LOGIN, data);
            string res = await responseMessage.Content.ReadAsStringAsync();
            Response<LoginResponse> response = JsonConvert.DeserializeObject<Response<LoginResponse>>(res);
            if (responseMessage.IsSuccessStatusCode && response.IsSuccess) {
                LoginResponse loginResponse = response.Data;
                Barrel.ApplicationId = "Cache";
                Barrel.Current.Add(key: "loginToken", data: response.Data, expireIn: TimeSpan.FromHours(2));
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MenuMePage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Error " + response.ErrorCode + ")", response.ErrorMessage, "OK", "cancel");
            }


        }

        public LoginWithCredentialsRequest NewLoginWithCredentialsRequest()
        {
            LoginWithCredentialsRequest loginRequest = new LoginWithCredentialsRequest();
            loginRequest.ClientId = "MOBILE";
            loginRequest.ClientSecret = "COURS";
            loginRequest.Login = userName;
            loginRequest.Password = password;
            return loginRequest;
        }
    }
}
