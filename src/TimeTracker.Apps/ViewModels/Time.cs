﻿using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace TimeTracker.Apps.ViewModels
{
    class Time : NotifierBase
    {
        private long id;
        private DateTime start_time;
        private  DateTime end_time;
        private TimeSpan total_sec;

        public ICommand DeleteCommand { get; set; }
        public ICommand ModifyCommand { get; set; }

        public Time(long idp,DateTime start_timep,DateTime end_timep)
        {
            id = idp;
            start_time = start_timep;
            end_time = end_timep;

            total_sec = end_timep - start_timep;
        }
        public void SetCommand(ICommand deleteCommand, ICommand modifyCommand)
        {
            DeleteCommand = deleteCommand;
            ModifyCommand = modifyCommand;
        }
        public long Id
        {
            get => id;
            set => SetProperty(ref id, value);
        }

        public TimeSpan Total_sec
        {
            get => total_sec;
            set => SetProperty(ref total_sec, value);
        }
        public DateTime Start_time
        {
            get => start_time;
            set => SetProperty(ref start_time, value);
        }

        public DateTime End_time
        {
            get => end_time;
            set => SetProperty(ref end_time, value);
        }
    }
}
