﻿using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;

using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    public class MenuMeViewModel : ViewModelBase
    {
        public ICommand MeCommand { get; }
        public ICommand ProjectCommand { get; }

        public ICommand TimerCommand { get; }

        public MenuMeViewModel(){
            MeCommand = new Command(GoToMeCommand);
            ProjectCommand = new Command(GoToProjectCommand);
            TimerCommand = new Command(StarNewTimer);


        }

        private void StarNewTimer()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<AddTimerPage>();
        }

        private void GoToProjectCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<MenuProjectPage>();
        }

        private void GoToMeCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<MePage>();
        }
    }
}
