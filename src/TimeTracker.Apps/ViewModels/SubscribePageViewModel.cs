﻿using Newtonsoft.Json;
using Storm.Api.Dtos;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Accounts;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class SubscribePageViewModel : ViewModelBase
    {
        private string email;
        private string firstName;
        private string lastName;
        private string password;
        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
        public string LastName
        {
            get => lastName;
            set => SetProperty(ref lastName, value);
        }
        public string Email {
            get => email;
            set => SetProperty(ref email, value);
        }
        public string FirstName
        {
            get =>firstName;
            set =>SetProperty(ref firstName,value);
        }
        public SubscribePageViewModel()
        {
            SubscribeCommand = new Command(CheckField);
            LoginCommand = new Command(GoToLoginCommand);
        }

        private async void GoToLoginCommand(object obj)
        {
            INavigationService nav = DependencyService.Get<INavigationService>();
            await nav.PushAsync<LoginPage>();
        }

        public ICommand SubscribeCommand{get;}
        private void CheckField()
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(password))
                App.Current.MainPage.DisplayAlert("Erreur", " Champ(s) non renseigné(s).", "ok");
            else
            {
                Subscribe();
            }
        }
        public async void Subscribe()
        {
            CreateUserRequest createUserRequest = InscriptionUser();
            var json = JsonConvert.SerializeObject(createUserRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            var responseMessage = await client.PostAsync(Urls.HOST + "/" + Urls.CREATE_USER, data);
            string res = await responseMessage.Content.ReadAsStringAsync();
            Response<UserProfileResponse> response = JsonConvert.DeserializeObject<Response<UserProfileResponse>>(res);
            if (responseMessage.IsSuccessStatusCode && response.IsSuccess)
            {
                INavigationService nav = DependencyService.Get<INavigationService>();
                await nav.PushAsync<MainPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Error " + response.ErrorCode + ")", response.ErrorMessage, "OK", "cancel");
            }
        }
        public CreateUserRequest InscriptionUser()
        {
            CreateUserRequest createUserRequest = new CreateUserRequest();
            createUserRequest.ClientId = "MOBILE";
            createUserRequest.ClientSecret = "COURS";
            createUserRequest.Email =email;
            createUserRequest.FirstName=firstName;
            createUserRequest.LastName=lastName;
            createUserRequest.Password=password;
            return createUserRequest;
        }

        public ICommand LoginCommand{get;}

    }

    

    
}
