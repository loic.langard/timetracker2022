﻿

using MonkeyCache.FileStore;
using Newtonsoft.Json;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using TimeTracker.Apps.Pages;
using TimeTracker.Dtos;
using TimeTracker.Dtos.Authentications;
using TimeTracker.Dtos.Projects;
using Xamarin.Forms;

namespace TimeTracker.Apps.ViewModels
{
    class AddProjectViewModel : ViewModelBase
    {
        private string description;
        private string name;
        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
        public ICommand GoBackCommand { get; }
        public ICommand SaveCommand { get; }
        public AddProjectViewModel()
        {
            GoBackCommand = new Command(GoToMenuProjectCommand);
            SaveCommand = new Command(SaveProjectCommand);
        }
        private void SaveProjectCommand()
        {
            if (string.IsNullOrEmpty(Description) || string.IsNullOrEmpty(Name))
                App.Current.MainPage.DisplayAlert("Erreur", " Champ(s) non renseigné(s).", "ok");
            else
            {
                SaveProject();
            }
        }
        private async void SaveProject()
        {
            HttpClient client = new HttpClient();
            LoginResponse loginresponse = Barrel.Current.Get<LoginResponse>(key: "loginToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginresponse.AccessToken);
            AddProjectRequest addPRojectRequest = NewAddProjectRequest();
            var json = JsonConvert.SerializeObject(addPRojectRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var responseMessage = await client.PostAsync(Urls.HOST + "/" + Urls.ADD_PROJECT, data);
            if (responseMessage.IsSuccessStatusCode)
            {
                INavigationService navigationService = DependencyService.Get<INavigationService>();
                await navigationService.PushAsync<MenuProjectPage>();
            }
            else
            {
                IDialogService dialogService = DependencyService.Get<IDialogService>();
                await dialogService.DisplayAlertAsync("Echec", "OK", "Cancel");
            }
            

        }
        private AddProjectRequest NewAddProjectRequest()
        {
            AddProjectRequest addProjectRequest = new AddProjectRequest();
            addProjectRequest.Name = name;
            addProjectRequest.Description = description;
            return addProjectRequest;
        }
        private void GoToMenuProjectCommand()
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PushAsync<MenuProjectPage>();
        }
    }
}
